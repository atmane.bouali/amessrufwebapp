package kbl.tmn.amssruf.dao;

import java.util.List;

import kbl.tmn.amssruf.domain.Expenditure;

public interface ExpenditureDAO {
	
	public void save(Expenditure expenditure);
	
	public void update(Expenditure expenditure);
	
	public void delete(Expenditure expenditure);
	
	public void delete(Integer expenditureId);
	
	public Expenditure findById(Expenditure expenditureId);
	
	public List<Expenditure> findAll();
	
	public List<Expenditure> findByProperty(String propName, Object propValue);
}

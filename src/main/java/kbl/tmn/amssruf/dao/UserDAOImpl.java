package kbl.tmn.amssruf.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import kbl.tmn.amssruf.domain.User;

@Repository
public class UserDAOImpl extends BaseDAO implements UserDAO{

	@Override
	public void save(User user) {
		String sql = "INSERT INTO `user` (name, phone, email, address, username, password, role, status)"
				+"VALUES (:name, :phone, :email, :address, :username, :password, :role, :status)";
		Map<String, Object> userMap = new HashMap<>();
		userMap.put("name", user.getName());
		userMap.put("phone", user.getPhone());
		userMap.put("email", user.getEmail());
		userMap.put("address", user.getAddress());
		userMap.put("username", user.getUsername());
		userMap.put("password", user.getPassword());
		userMap.put("role", user.getRole());
		userMap.put("status", user.getStatus());
		
		KeyHolder kh = new GeneratedKeyHolder();
		SqlParameterSource ps = new MapSqlParameterSource(userMap);
		super.getNamedParameterJdbcTemplate().update(sql,ps, kh);
		Integer userIdGeneratedInteger = kh.getKey().intValue();
		user.setId(userIdGeneratedInteger);
	}

	@Override	
	public void update(User user) {
		String sql = "UPDATE user SET"
				+" name = :name,"
				+" phone = :phone,"
				+" email = :email,"
				+" address = :address,"
				+" role = :role,"
				+" status = :status"
				+" WHERE id  = :user_id";
	
		Map<String, Object> userMap = new HashMap<>();
		userMap.put("user_id", user.getId());
		userMap.put("name", user.getName());
		userMap.put("phone", user.getPhone());
		userMap.put("email", user.getEmail());
		userMap.put("address", user.getAddress());
		userMap.put("role", user.getRole());
		userMap.put("status", user.getStatus());
		super.getNamedParameterJdbcTemplate().update(sql, userMap);
	}

	@Override
	public void delete(User user) {
		this.delete(user.getId());
		
	}

	@Override
	public void delete(Integer userId) {
		String sql = "DELETE FROM user WHERE id = ?";
		super.getJdbcTemplate().update(sql, userId);
	}

	@Override
	public User findById(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findByProperty(String propName, Object propValue) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}

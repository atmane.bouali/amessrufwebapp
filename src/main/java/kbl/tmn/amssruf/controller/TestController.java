package kbl.tmn.amssruf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {
	
	@RequestMapping("/test/hello")
	public String helloWord() {
		return "hello";
	}
}

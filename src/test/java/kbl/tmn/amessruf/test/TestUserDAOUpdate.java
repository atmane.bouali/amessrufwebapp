package kbl.tmn.amessruf.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import kbl.tmn.amssruf.config.SpringRootConfig;
import kbl.tmn.amssruf.dao.UserDAO;
import kbl.tmn.amssruf.domain.User;

public class TestUserDAOUpdate {

	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringRootConfig.class);
		UserDAO userDAO = ctx.getBean(UserDAO.class);
		User updatedUser = new User();
		updatedUser.setId(2);
		updatedUser.setName("AOUCHAR Katia");
		updatedUser.setPhone("06666666");
		updatedUser.setEmail("katia.aouchar06@gmail.com");
		updatedUser.setAddress("2 impasse des orteaux 75020 CHEZ BOAULI BELKACEM");
		updatedUser.setRole(2);
		updatedUser.setStatus(1);
		userDAO.update(updatedUser);
		System.out.println("***************** USER DATA UPDATE *********************");
	}

}

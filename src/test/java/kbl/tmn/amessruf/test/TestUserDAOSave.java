package kbl.tmn.amessruf.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import kbl.tmn.amssruf.config.SpringRootConfig;
import kbl.tmn.amssruf.dao.UserDAO;
import kbl.tmn.amssruf.domain.User;

public class TestUserDAOSave {

	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringRootConfig.class);
		UserDAO userDAO = ctx.getBean(UserDAO.class);
		User newUser = new User();
		newUser.setName("BOUALI Katia");
		newUser.setPhone("0664419876");
		newUser.setEmail("katia.aouchar06@gmail.com");
		newUser.setAddress("2 impasse des orteaux 75020");
		newUser.setUsername("Katia");
		newUser.setPassword("password2");
		newUser.setRole(2);
		newUser.setStatus(0);
		userDAO.save(newUser);
		System.out.println("***************** USER DATA SAVE *********************");
		System.out.println(" Generated ID iS = "+ newUser.getId());
	}

}

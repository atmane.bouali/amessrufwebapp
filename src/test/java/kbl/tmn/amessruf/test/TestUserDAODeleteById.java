package kbl.tmn.amessruf.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import kbl.tmn.amssruf.config.SpringRootConfig;
import kbl.tmn.amssruf.dao.UserDAO;
import kbl.tmn.amssruf.domain.User;

public class TestUserDAODeleteById {

	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringRootConfig.class);
		UserDAO userDAO = ctx.getBean(UserDAO.class);
		userDAO.delete(2);
		System.out.println("***************** USER DELETE BY ID *********************");
	}

}

package kbl.tmn.amessruf.test;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import kbl.tmn.amssruf.config.SpringRootConfig;

public class TestDataSource {

	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringRootConfig.class);
		DataSource ds = ctx.getBean(DataSource.class);
		JdbcTemplate jt = new JdbcTemplate(ds);
		String sql = "INSERT INTO `user` (`name`, `phone`, `email`, `address`, `username`, `password`, `role`, `status`) VALUES (?,?,?,?,?,?,?,?)";
		Object [] params = new Object[] {"BOUALI Atmane", "0664419685", "bouali.atmane06@gmail.com", "2 impasse des orteaux 75020 Paris", "atmane", "password", 1, 1};
		jt.update(sql, params);
		System.out.println("************SQL executed***************");
	}

}
